Quirks and caveats
==================

Using with GROMACS
------------------

#. Take care that the updated ``residuetypes.dat`` is present in your working directory

#. Because the greek τ letter can be present in β-amino acids (e.g. N\ :sub:`τ1` and N\ :sub:`τ2` if an arginine
   side-chain is substituted in the β\ :sup:`3` position), they can clash with the ``NT*`` and ``OT*`` atoms of various
   C-terminal groups. To resolve this, we have renamed the corresponding terminal atoms to ``NW*`` and ``OW*``.

#. If ``gmx pdb2gmx`` complains about missing atoms, run it with the ``-v`` option, and look for automatic atom renaming
   (e.g. because of the ``xlateat.dat`` file). Sometimes overriding the system-wide atom renaming file in the current
   working directory can help.

#. Avoid storing molecular geometries of beta-peptides in PDB files for two reasons:
    #. the length of residue names is limited to 3 characters (four is non-standard)
    #. the precision of the coordinates is limited
   The .g96 file format is preferred:
    #. it supports 5 character residue names
    #. can store coordinates (even velocities) in high precision
    #. supports multi-model files as PDB
    #. PyMOL can load it easily
    #. in contrast to its name, ``gmx pdb2gmx`` accepts it as an input file

