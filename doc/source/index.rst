.. charmm36m-beta documentation master file, created by
   sphinx-quickstart on Mon May  6 16:02:14 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to charmm36m-beta's documentation!
==========================================

This is an extended version of the CHARMM36m force field which can reliably handle the folding properties of acyclic
β-peptides. More information in the original paper [Wacha2019]_.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   betapeptides
   quickstart
   betaresidues
   parametrization
   caveats
   others
   bugs
   disclaimer


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
