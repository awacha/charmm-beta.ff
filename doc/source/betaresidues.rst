β-residues
==========

Derivation from CHARMM amino acid topologies
--------------------------------------------

For the convenience of prospective users, we have generated residue topologies for mono- and disubstituted β-amino
acids with proteinogenic side-chains, as well as the β-alanine, employing analogies with α-amino acids wherever
possible:

1. The atom types for the peptide bond are kept the same (i.e. ``NH1``, ``O``, ``C`` and ``H``)

2. New atom types were introduced for the α- and β-carbons:

    ========= ======== ============================
    Atom type Based on Usage
    ========= ======== ============================
    ``CTA1``  ``CT1``  β-carbon with side-chain
    ``CTA2``  ``CT2``  α-carbon without side-chain
    ``CTB1``  ``CT1``  α-carbon with side-chain
    ``CTB2``  ``CT2``  β-carbon without side-chain
    ========= ======== ============================

3. Side-chain atoms were taken from the α-counterparts:

    ======= ================= =======================================
    Letter    CHARMM36 base     Description
    ======= ================= =======================================
      A           ALA           alanine
      C           CYS           cysteine (neutral)
      CM          CYM           cysteine (anionic)
      D           ASP           aspartate (ionic)
      DH          ASPP [#p]_    aspartic acid (protonated)
      E           GLU           glutamate (ionic)
      EH          GLUP [#p]_    glutamic acid (protonated)
      F           PHE           phenyl-alanine
      G           GLY           glycine (i.e. no side-chain)
      HD          HSD           neutral histidine, protonated on Nδ
      HE          HSE           neutral histidine protonated on Nε
      HH          HSP           protonated histidine
      I           ILE           isoleucine
      K           LYS           lysine (charged)
      KN          LSN [#p]_     lysine (neutral)
      L           LEU           leucine
      M           MET           methionine
      N           ASN           asparagine
      O           ORP [#c]_     ornithine (charged)
      ON          ORN [#c]_     ornithine (neutral)
      Q           GLN           glutamine
      R           ARG           arginine
      S           SER           serine
      T           THR           threonine
      V           VAL           valine
      W           TRP           tryptophan
      Y           TYR           tyrosine
    ======= ================= =======================================

.. [#p] A patch residue in CHARMM36m

.. [#c] Does not exist in CHARMM36m, created by removing a methylene from lysine

4. Partial charges and charge groups of backbone atoms have been assigned as follows:
    #. the backbone is divided into three charge groups:
        - N, H, CB, HB*
        - CA, HA*
        - C, O
    #. partial charges of the peptide bond:
        - peptide N: -0.470
        - peptide C:  0.51
        - peptide O: -0.51
        - peptide H:  0.310
    #. the partial charge of all aliphatic hydrogens is 0.09.
    #. partial charges on CB and CA are determined by requiring all charge groups to be neutral. I.e.:
        - β\ :sup:`3` substituted amino-acids (i.e. β\ :sup:`3` and β\ :sup:`2,3`): CB -> 0.07
        - not β\ :sup:`3` substituted amino-acids (i.e. βA and β\ :sup:`2`): CB -> -0.02
        - β\ :sup:`2` substituted amino-acids (i.e. β\ :sup:`2` and β\ :sup:`2,3`): CA -> -0.09
        - not β\ :sup:`2` substituted amino-acids (i.e. βA and β\ :sup:`3`): CB -> -0.18


Naming conventions
------------------

Residue names have been assigned according to the substitution:

    - βA is ``BALA``
    - β\ :sup:`3`-amino acids: ``B3*``, e.g. ``B3A``, ``B3C``, ``B3KN`` etc.
    - β\ :sup:`2`-amino acid:   ``B2*``, e.g. ``B2A``, ``B2C``, ``B2KN`` etc.
    - beta\ :sup:`2,3`-amino acids: a serious limitation of CHARMM is that residue names contain at most 4 uppercase
      alphanumeric characters (i.e. A-Z and 0-9). For residues where the side-chains can be represented with
      a single letter, we chose the ``B0XY`` notation, as it does not clash with any existing residue name. Thus,
      for example: β\ :sup:`2,3`-h(2Ala,3Phe) will be ``B0AF`` etc. Due to the limitation above, currently there is
      no way to represent disubstituted β-amino acids with alternate charge/protonation states.
    - No distinction is made in chirality either in residue or atom names. This has to be resolved in the molecular
      geometry.

Atom names mainly follow the convention of CHARMM, i.e.:
    #. the atoms of the peptide bond are named ``N``, ``HN``, ``C`` and ``O``
    #. heavy (non-hydrogen) atoms are assigned Greek letters according to their distance from ``C``, in the following
       sequence: ``ABGDEZHT``
    #. when there are multiple heavy atoms with the same Greek letter, a number is added, according to the following
       rules:

        #. the β\ :sup:`3` side-chain takes precedence over the β\ :sup:`2` side-chain (i.e. the backbone β-carbon is
           always either ``CB`` or ``CB1``)
        #. atoms belonging to the same side-chain inherit their precedence from the original CHARMM topology

       Thus the name of a non-peptide-bond heavy atom will conform the scheme
       ``<element><Greek letter>[<optional number>]``, i.e. ``CB1`` or ``NE2``

    #. hydrogen atoms are labeled after their ligands, by changing the first letter to ``H``. If there are more
       hydrogens attached to the same atom, an additional number is appended. In case of side-chain atoms, the order
       is inherited from the CHARMM topology. In the backbone protons, ``S`` absolute conformation should take
       recedence over ``R``
